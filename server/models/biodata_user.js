'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class biodata_user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  biodata_user.init({
    biodata_id: DataTypes.INTEGER,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    alamat: DataTypes.STRING,
    nohp: DataTypes.INTEGER,
    jenisKelamin: DataTypes.STRING,
    diamond: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'biodata_user',
  });
  return biodata_user;
};