import React, { Fragment } from 'react';
import { Container, Jumbotron } from 'reactstrap';
import Header from './Header/Header';
import './Home.css'
import Main from './Main/Main';
import Main2 from './Main/Main2';

const Home = () => {
    return(
        <Fragment>
            <section>
            <Container fluid className="main">
            <Container fluid className="container-navbar">
                <Header/>
            </Container>
                <Main/>
            </Container>
            </section>
            <Container fluid className='hr'></Container>
            <section>
                <Main2/>
            </section>
        </Fragment>
        
    )
}

export default Home