import React from 'react';
import { Container } from 'reactstrap';
import Corousel from '../../../../Components/Atoms/Corousel';
import './Main2.css'

const Main2 = () => {
    return(
        <Container fluid className="page2">
            <Container className="main2">
                <aside>What's so special?
                <nav>the games</nav>

                </aside>
                <main>
                    <Container className='corousel'>
                    <Corousel/>
                    </Container>
                </main>
            </Container>
        </Container>
    )
}

export default Main2