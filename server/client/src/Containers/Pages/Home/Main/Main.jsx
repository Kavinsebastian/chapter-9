import React from 'react';
import { Container, Button } from 'reactstrap';
import './Main.css'
import img from '../../../../Assets/img/background/pngkin.com_black-triangle-png_433909.png'

const Main = () => {
    return(
            <Container className="content">
                <h1>play traditional game</h1>
                <p>Experience new Traditional game play</p>
                <main><Button className="play" color="warning">play now</Button></main>
                <Container className='triangle'>
                <p>the story</p>
                <footer>
                    <img className="icon" src={img} alt="" />
                    </footer>
                    </Container>
                    
            </Container>
    )
}

export default Main