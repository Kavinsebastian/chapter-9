import React,{useState} from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Container,
} from 'reactstrap';

import './Navbar.css'


const NavbarComp = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Container className="navs">
            <Navbar dark color="transparent" light expand="xl">
            <NavbarBrand href="/">Gemology</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <NavItem>
                <NavLink href="#">HOME</NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="#">WORK</NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="#">CONTACT</NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="#">ABOUT ME</NavLink>
                </NavItem>
                <NavItem>
                <NavLink className="reg" href="#">SING UP</NavLink>
                </NavItem>
                <NavItem>
                <NavLink  href="#">LOGIN</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                </UncontrolledDropdown>
            
            </Collapse>
            </Navbar>
            </Container>
    );
}

export default NavbarComp